

```shell
# build
javac Main.java
jar cfm main.jar manifest.mf *.class

# publish to production

./funky.cli-macos --path=./ \
  --function=ola \
  --branch=master \
  --token=SPACECALAMAR \
  --platform=http://funky.eu.ngrok.io \
  --cmd=deploy



curl --header "ADMIN_TOKEN: SPACECALAMAR" \
  -F "function=ola" \
  -F "branch=master" \
  -F "file=@./ola-master/config.yml" \
  -F "file=@./main.jar" \
  http://funky.test:9090/publish

# invoke
curl http://funky.test:9090/function/ola/master
```

```shell
# build
javac Main.java
jar cfm main.jar manifest.mf *.class
# publish
curl --header "ADMIN_TOKEN: SPACECOW" \
  -F "function=ola" \
  -F "branch=dev" \
  -F "file=@./config.yml" \
  -F "file=@./main.jar" \
  http://funkydev.test:9090/publish

# invoke
curl http://funkydev.test:9090/function/ola/dev
```

