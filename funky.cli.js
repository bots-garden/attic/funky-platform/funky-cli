const argv = require('yargs').argv
const fs = require('fs')
const path = require('path')
const chalk = require('chalk')
const cp = require('child_process')

/*
node funky.cli.js --path=./ola-function \
  --function=ola \
  --branch=master \
  --token=SPACESQUID \
  --platform=http://funky.eu.ngrok.io \
  --cmd=deploy
*/
// --- deploy lambda ---
let deploy = ({functionPath, functionName, branchName, token, platform}) => {

  //console.log({functionPath, functionName, branchName, token, platform})

  let files = fs.readdirSync(`${functionPath}`)
    .filter(
      file=> path.extname(file) == ".jar" || path.extname(file) == ".yml"
    )
  console.log(chalk.blue(`function: ${functionName}`))
  console.log(chalk.green(`branch: ${branchName}`))
  console.log(chalk.yellow(`files to upload: ${files}`))

  files.forEach(file => {

    let command = `
    curl --header "ADMIN_TOKEN: ${token}" \
      -F "function=${functionName}" \
      -F "branch=${branchName}" \
      -F "file=@${functionPath}/${file}" \
      ${platform}/publish
    `

    cp.exec(`${command}`, (error, stdout, stderr) => {
      if(error) {
        console.log(chalk.red(stderr))
      } else {
        console.log(stdout)
      }
    })
      
  })
  console.log(chalk.green(`function: ${functionName} uploaded`))
  console.log(chalk.yellow(`invoke: curl ${platform}/function/${functionName}/${branchName}`)) 
}
// --- end of deploy lambda ---

/*

node funky.cli.js \
  --function=ola \
  --branch=master \
  --token=SPACESQUID \
  --platform=http://funky.eu.ngrok.io \
  --cmd=remove
*/
// --- remove lambda ---
let remove = ({functionName, branchName, token, platform}) => {
  console.log(chalk.blue(`function: ${functionName}`))
  console.log(chalk.green(`branch: ${branchName}`))

  let command = `
  curl --header "ADMIN_TOKEN: ${token}" \
    -X "DELETE" \
    ${platform}/function/${functionName}/${branchName}  
  `

  cp.exec(`${command}`, (error, stdout, stderr) => {
    if(error) {
      console.log(chalk.red(stderr))
    } else {
      console.log(stdout)
    }
  })
  console.log(chalk.green(`function: ${functionName} removed`))
}
// --- end of remove lambda ---

// read the parameter from the command line
let functionName = argv.function
let branchName = argv.branch
let token = argv.token
let platform = argv.platform
let cmd = argv.cmd

if(functionName && branchName && token && platform && cmd) {
  try {

    switch (cmd) {
      case "deploy":
        deploy({
          functionPath: argv.path, 
          functionName: functionName,
          branchName: branchName,
          token: token,
          platform: platform
        })
        break;
      case "remove":
        remove({
          functionName: functionName,
          branchName: branchName,
          token: token,
          platform: platform
        })
        break;
      default:
        console.log("🤔 🚧 work in progress...")
    }

  } catch(error) {
    console.log(chalk.red(error))
  }
} else {
  // --- there is a problem with the parameters
  console.log(chalk.red(`function: ${functionName}`))
  console.log(chalk.red(`branch: ${branchName}`))
  console.log(chalk.red(`token: ${token}`))
  console.log(chalk.red(`platform: ${platform}`))
}
